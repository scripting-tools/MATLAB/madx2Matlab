function [ coefficients, errors, rsquare ] = gaussianFit( xArray, yArray )
%GAUSSIANFIT It makes a gaussian fit of a profile and returns the main
%informations of it with a goodness of the fit (r^2.) 
%The errors on the computed quantities are expected to be sigmas. 95%
%confidence level is then a range +-2 times the value given.

[auxFitObject, auxGoodness] = fit(xArray(:),yArray(:),'gauss1');
auxCoefficients = confint( auxFitObject, 0.95);
    
coefficients = [auxFitObject.a1, auxFitObject.b1, auxFitObject.c1/sqrt(2)];
errors = (auxCoefficients(2,:) - auxCoefficients(1,:))/4;
errors(3) = errors(3)/sqrt(2);
rsquare = auxGoodness.rsquare;
end

