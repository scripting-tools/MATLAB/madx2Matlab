function [ totEmittance, meanX, meanAngle, totAlpha, totBeta ] = sumEllipses( emittances, xCenters, angleCenters, alphas, betas, weights )
%[ totEmittance, meanX, meanAngle, totAlpha, totBeta ] = sumEllipses( emittances, xCenters, angleCenters, alphas, betas, weights )
%   given an array of beam phase spaces (i.e. their emittance, alpha, 
%   beta and center), it will generate the equivalent ellipse that
%   represents all of them.
%
% davideg - July 2014

emittances = emittances(:);
xCenters = xCenters(:);
angleCenters = angleCenters(:);
alphas = alphas(:);
betas = betas(:);
% if nargin == 5
%     weights = ones(numel(emittances),1);
% end
weights = weights(:);

% check dimenstions
if ((numel(emittances) ~= numel(xCenters))  || ...
    (numel(emittances) ~= numel(angleCenters)) || ...
    (numel(emittances) ~= numel(alphas)) || ...
    (numel(emittances) ~= numel(betas))  || ...
    (numel(emittances) ~= numel(weights)))
    error('sumEllipses::wrong size of inputs. check your code!');
end
    
% normalize weights
sumWeights = sum(weights);
weights = (weights./sumWeights)';


% calculate sigmas 
sigmaXesSquare = betas.*emittances;
sigmaAnglesSquare = (1+alphas.^2).*emittances./betas;

% total means are easy
meanX = weights*xCenters;
meanAngle = weights*angleCenters;

% stds more complicate
stdXSquare = weights*(sigmaXesSquare+xCenters.^2) - meanX^2;
stdAngleSquare = weights*(sigmaAnglesSquare+angleCenters.^2) - meanAngle^2;
covarXAngle = weights*(-alphas.*emittances + xCenters.*angleCenters) - meanX*meanAngle;


totEmittance = sqrt(stdXSquare*stdAngleSquare - covarXAngle^2);
totBeta = stdXSquare/totEmittance;
totAlpha = -covarXAngle/totEmittance;

end

