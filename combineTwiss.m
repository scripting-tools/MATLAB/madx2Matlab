function [betaTot, alphaTot, emitTot, centreTot, angleTot] = ...
    combineTwiss( betas, alphas, emits, centres, angles, weights )
%[betaTot, alphaTot, emitTot, centreTot, angleTot] = combineTwiss( betas, alphas, emits, centres, angles, weights )
%
%   This function is a wrapper of combineSigmas.m function.
%   The idea is to imput a list of Twiss measurements of different beam
%   that are combined, and give the combined Twiss paraemters and centres.
%

% some default parameters
if nargin == 3
    centres = zeros(size(betas));
    angles = zeros(size(betas));
    weights = ones(1,length(betas));
elseif nargin == 5
    weights = ones(1,length(betas));
end

% generate sigmas/centres for all Twiss measured
auxSigmas  = cell(length(betas), 1);
auxCentres = zeros(length(betas),2);
for i=1:length(betas)
    auxGamma = (1+alphas(i)^2)/betas(i);
    auxSigmas{i} = emits(i) .* [betas(i) -alphas(i); -alphas(i) auxGamma];
    auxCentres(i,1) = centres(i);
    auxCentres(i,2) = angles(i);
end

% call the combineSigmas function and extract interesting parameters.
[tot_sigma, tot_centers] = combineSigmas( auxSigmas, auxCentres, weights );
emitTot = sqrt(det(tot_sigma));
betaTot = tot_sigma(1)/emitTot;
alphaTot = -tot_sigma(2)/emitTot;
centreTot = tot_centers(1);
angleTot = tot_centers(2);

end

