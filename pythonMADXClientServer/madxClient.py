#!/usr/bin/python           
#
# python client to contact madxServer.py.
# See server code for description.
# One should just modify here the serverHost/port variables to contact
# proper server
#
# April 2014 - davide.gamba@cern.ch

import socket               # Import socket module
import struct
import sys

# configuration variables
serverHost = 'localhost';
serverPort = 5061;


###########################
# some function definition

# function to send data over a connection
def mySendData(the_socket, data):
    # from http://code.activestate.com/recipes/408859/
    the_socket.sendall(struct.pack('>i', len(data))+data)

# function to receive data over a connection
def myReceiveData(the_socket):
    # from http://code.activestate.com/recipes/408859/
    #data length is packed into 4 bytes
    total_len=0;total_data=[];size=sys.maxint
    size_data=sock_data='';recv_size=8192
    while total_len<size:
        sock_data=the_socket.recv(recv_size)
        if not total_data:
            if len(sock_data)>4:
                size_data+=sock_data
                size=struct.unpack('>i', size_data[:4])[0]
                recv_size=size
                if recv_size>524288:recv_size=524288
                total_data.append(size_data[4:])
            else:
                size_data+=sock_data
        else:
            total_data.append(sock_data)
        total_len=sum([len(i) for i in total_data ])
    return ''.join(total_data)



###########################
# start of the real script

# read data from stdin
scriptLines = sys.stdin.readlines()
scriptLines = ''.join(scriptLines)


# create socket connection
mysocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)           # Create a socket object
mysocket.connect((serverHost, serverPort))

# send script lines
mySendData(mysocket, scriptLines)

# get output
myoutput = myReceiveData(mysocket)

# close connection
mysocket.close

# check output
if myoutput == '-1':
  sys.exit("Some error occurred on the madx server... :(")

# print it to stdoutput
print myoutput

